<?php
$resn = $this->resultSet['result']['news'];
?>
<div class="col-xs-12 col-md-10">
	<?php
	$count = count($resn);
	if($count==0)
	{
		?>
		<h3>Sorry, No news found</h3>
		<?php
	}
	else
	{
		?>
		<div itemscope itemtype="http://schema.org/Article">
			<?php if(!empty($resn[0]['post_released'])) {?><p class="released"><?php echo stripslashes($resn[0]['post_released']);?></p><?php }?>
			<h3 itemprop="name" class="<?php if(empty($resn[0]['post_released'])) echo "title-only"; ?>"><?php echo stripslashes($resn[0]['post_title']);?></h3>
			<?php if(!empty($resn[0]['post_heading'])) {?><h5><?php echo stripslashes($resn[0]['post_heading']);?></h5><?php }?>
			<?php if(!empty($resn[0]['post_subheading'])) {?><h3><?php echo stripslashes($resn[0]['post_subheading']);?></h3><?php }?>
			<div itemprop="articleBody">
				<?php echo $resn[0]['post_cms']; // makeChart(cleanMyCkEditor($resn[0]['post_cms']));?>
			</div>
			<div class="addthis_sharing_toolbox" style="float: right"></div>
			<meta itemprop="articleSection" content="Japan Economy">
			<meta itemprop="url" content="http:/<?php echo $_SERVER['REQUEST_URI'];?>">
			<span itemprop="author" itemscope itemtype="http://schema.org/Person">
				<meta itemprop="datePublished" content="<?php echo date(strtotime($resn['post_datetime']));?>">
				<meta itemprop="name" content="Takuji Okubo">
			</span>
			<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
				<meta itemprop="name" content="Japanmacroadvisors">
			</span>
		</div>
	</div>
	<?php
}
?>
<?php
 // include('view/templates/rightside.php');
?>