<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="description" content="<?php echo $this->resultSet['meta']['description'];?>">
	<meta name="keywords" content="<?php echo $this->resultSet['meta']['keywords'];?>">
	<meta name="google-translate-customization" content="1fea04e055fb6965-35248e5248638537-g6177b01b3439e3b2-16"></meta>
	<meta property="og:type" content="article" />
	<meta property="og:image" content="http://content.japanmacroadvisors.com/images/japan-macro-advisors.png" />
	<meta property="og:site_name" content="japanmacroadvisors.com" />
	<meta property="og:url" content="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" />
	<meta property="og:title" content="<?php echo !isset($this->resultSet['meta']['shareTitle']) ? 'Japan economy | Macro economy | Economist - GDP, Inflation - Analysis on Japanese economy by Mr. Takuji Okubo' : $this->resultSet['meta']['shareTitle']; ?>" />
	<meta property="og:description" content="<?php echo $this->resultSet['meta']['description'];?>" />
	<meta property="fb:app_id" content="1597539907147636" />
	<base href="<?php echo $this->rootPath; ?>">
	<title><?php echo !isset($this->pageTitle) ? 'Indian economy | Macro economy | Economist - GDP, Inflation - Analysis on Japanese economy by Mr. Takuji Okubo' : $this->pageTitle; ?></title>
	<link rel="shortcut icon" href="favicon.png" type="image/icon">
	<link rel="icon" href="favicon.png" type="image/icon">
	
	
	
	<script src="<?php echo $this->javascript."mapStock.js";?>"></script>
	<script src="<?php echo $this->javascript."map.js";?>"></script>
	<script src="<?php echo $this->javascript."indiamap.js";?>"></script>


	<!-- fontawesome -->
	<link rel="stylesheet" href="<?php  echo $this->assets."plugins/font-awesome/css/font-awesome.css";?>">
	<!-- bootstrap -->
	<link rel="stylesheet" href="<?php  echo $this->assets."plugins/bootstrap/css/bootstrap.css";?>">
	
	<link href="<?php  echo $this->css."jquery.alerts.css";?>" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->css."intlTelInput.css";?>" />
	<link rel="stylesheet" media="print" href="<?php echo $this->css."print_page.css";?>" />
	<script type="text/javascript" src="<?php echo $this->javascript."jquery.min.js";?>"></script>
	<script src="<?php echo $this->javascript."bootstrap.min.js";?>"></script>
	<script type="text/javascript" src="<?php echo $this->javascript."jma.js";?>"></script>
	<script type="text/javascript" src="<?php echo $this->javascript."jquery.easing-1.3.pack.js";?>"></script>
	<script type="text/javascript" src="<?php echo $this->javascript."Sortable.min.js";?>"></script>
	<script type="text/javascript" src="<?php echo $this->javascript."handlebars-v2.0.0.js";?>"></script>


	<!--  -->
	
		 
	
	
	

	<script type="text/javascript" src="<?php echo $this->javascript."jquery.validate.js";?>"></script>
	<script type="text/javascript" src="<?php echo $this->javascript."jspdf.min.js";?>"></script>
   

	<script type="text/javascript" src="<?php echo $this->javascript."intlTelInput.min.js";?>"></script>
	<script type="text/javascript" src="<?php echo $this->javascript."jquery.alerts.js";?>"></script>
	<?php echo $this->getAllJavascript(); ?>
	<script type="text/javascript" src="<?php echo $this->javascript."jquery.cookiebar.js";?>"></script>

	<link rel="stylesheet" href="<?php echo $this->assets."css/custom-styles.css";?>" />
	<link rel="stylesheet" href="<?php echo $this->assets."css/media.css";?>" />
	<?php echo $this->getAllCss(); ?>
	<script type="text/javascript">
	
	
	
	var objectParams = {
		myChart : {
			folderList : <?php echo json_encode($this->resultSet['result']['category']['folderList']);?>
		}
	};
	var JMA = new Jma('<?php echo $this->rootPath; ?>','<?php echo $this->controllername; ?>','<?php echo $this->actionname; ?>','<?php echo is_array($this->params) ? implode('/', $this->params) : ''; ?>','<?php echo isset($_SERVER['HTTPS']) ? 'https' : 'http'?>',objectParams);
	<?php
	if (isset ( $_SESSION ['user'] ) && $_SESSION ['user'] ['id'] > 0) {
		?>
		JMA.userDetails = <?php echo json_encode($_SESSION['user']);?>;
		<?php
	}
	?>
	</script>

</head>
<body>
	<?php
	$ENV = Config::read('environment');
	if($ENV == 'production') {
		?>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NX7MF9"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NX7MF9');</script>
		<!-- End Google Tag Manager -->
		<?php
	} elseif ($ENV == 'test'){
		?>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KGR56S"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KGR56S');</script>
		<!-- End Google Tag Manager -->
		<?php
	}
	?>
	<!-- <div class="container"> -->
	<div id="overlay_loading">
		<div class="cssload-preloader">
			<div class="cssload-preloader-box">
				<div>L</div>
				<div>o</div>
				<div>a</div>
				<div>d</div>
				<div>i</div>
				<div>n</div>
				<div>g</div>
			</div>
		</div>
	</div>
	<!-- <div class="payloder">
		<div id="spinner">
			<i style="color:red;" class="fa fa-spinner fa-spin fa-5" ></i>
		</div>
	</div> -->
	<!-- Templates Start -->
	<script type="template/alanee" id="template_graph_full">

	
	<div class="h_graph_wrap notranslate pad0 {{#if chart_details.isRightPannel}}col-lg-8 col-xs-12{{else}}{{/if}}" id="h_graph_wrap_{{chart_details.chartIndex}}">
	<div class="h_graph_graph_area">
	<div class="h_graph_top_area">
	{{#if chart_details.isRightPannel}}
	{{#unless mychart_details.isMyChart}}
	<div class="EDHeading">
	<ul class="list-inline list_graphhead">
	<li class="nav_edittabs">
	<div class="ExportHeading graph-nav">
	<div class="nav-txt nav-txt-export"> 
	<i class="fa fa-pencil" style="color:#e60013"></i>&nbsp;Edit
	</div>
	</li>
	<li>
	<div class="ExportHeading graph-nav" id="EXDOW" >
	<div class="nav-txt nav-txt-export"> 
	<i class="fa fa-download" style="color:#e60013"></i>&nbsp;EXPORT
	</div>
	<div class="Exports sub-nav">
	<div id="FloatLeft"></div>
	<div id="" class="ExportItem1">
	<select id="export_chart_image_select_format_{{chart_details.chartIndex}}" class="addmore-select form-control">
	<option value="csv">Data (CSV)</option>
	<option value="jpeg">Image (JPEG)</option>
	<option value="png">Image (PNG)</option>
	<option value="pdf">Document (PDF)</option>
	<option value="ppt">PowerPoint (PPTX)</option>
	</select>
	<input type="button" class="btn btn-primary btn-sm" value="Export" onClick="JMA.JMAChart.exportChart({{chart_details.chartIndex}});">
	</div>
	</div>
	</div>
	</li>
	<li>
	<div class="ExportHeading graph-nav" id="EXDOW" >
	<div class="nav-txt nav-txt-save"> <i class="fa fa-line-chart" style="color:#e60013"></i>&nbsp;SAVE</div>
	<div class="Folders sub-nav">
	<div id="FloatLeft"></div>
	<div id="" class="ExportItem1">
	<select id="save_chart_select_folder_{{chart_details.chartIndex}}" class="addmore-select form-control mychart-select-addto-folder">
	{{#mychart_details.folderList}}
	<option value="{{folder_id}}">{{folder_name}}</option>
	{{/mychart_details.folderList}}
	</select>
	<input type="button" class="btn btn-primary btn-sm" value="Save" onClick="JMA.myChart.saveThisChartToFolder({{chart_details.chartIndex}});">
	</div>
	</div>
	</div>
	</li>
	</ul>
	</div>
	
	</div>
	{{/unless}}
	{{else}}
	{{#unless mychart_details.isMyChart}}
	<div class="EDHeading">
	<ul class="list-inline list_graphhead">
	<li>
	<div class="ExportHeading graph-nav" id="EXDOW" >
	<a href="javascript:;" class="nav-txt nav-txt-export"><i class="fa fa-file-image-o"></i> EXPORT</a>
	<div class="Exports sub-nav">
	<div id="FloatLeft"></div>
	<div id="" class="ExportItem1">
	<select id="export_chart_image_select_format_{{chart_details.chartIndex}}" class="addmore-select form-control">
	<option value="csv">Data (CSV)</option>
	<option value="jpeg">Image (JPEG)</option>
	<option value="png">Image (PNG)</option>
	<option value="pdf">Document (PDF)</option>
	<option value="ppt">PowerPoint (PPTX)</option>
	</select>
	<input type="button" class="btn btn-primary btn-sm" value="Export" onClick="JMA.JMAChart.exportChart({{chart_details.chartIndex}});">
	</div>
	<div id="" class="ExportItem2" style="display:none">
	<select id="export_chart_image_size_{{chart_details.chartIndex}}" class="addmore-select form-control">
	<option value="small">Small</option>
	<option value="medium">Medium</option>
	<option value="large">Large</option>
	</select>
	<input type="button" class="btn btn-primary btn-sm" value="Export" onClick="JMA.JMAChart.exportChart({{chart_details.chartIndex}});">
	</div>
	</div>
	</div>
	</li>
	<!-- <li>
	<div class="DownloadHeading graph-nav" id="EXDOW">
	<a href=""><i class="fa fa-download"></i> DOWNLOAD</a>
	<div class="Downloads sub-nav">
	<select id="download_data_select_format_{{chart_details.chartIndex}}" class="addmore-select form-control" >
	<option value="csv">Comma seperated Value (CSV)</option>
	</select>
	<input type="button"  class="btn btn-primary btn-sm" value="Download" onClick="JMA.JMAChart.downloadChartData({{chart_details.chartIndex}});" />
	</div>
	</div>
	</li> -->
	<li>
	<div class="ExportHeading graph-nav" id="EXDOW" >
	<a href="javascript:;" class="nav-txt nav-txt-save"><i class="fa fa-line-chart"></i> SAVE</a>
	<div class="Folders sub-nav">
	<div id="FloatLeft"></div>
	<div id="" class="ExportItem1">
	<select id="save_chart_select_folder_{{chart_details.chartIndex}}" class="addmore-select form-control mychart-select-addto-folder ">
	{{#mychart_details.folderList}}
	<option value="{{folder_id}}">{{folder_name}}</option>
	{{/mychart_details.folderList}}
	</select>
	<input type="button" class="btn btn-primary btn-sm" value="Save" onClick="JMA.myChart.saveThisChartToFolder({{chart_details.chartIndex}});">
	</div>
	</div>
	</li>
	</ul>
	</div>
	</div>
	{{/unless}}
	{{/if}}
	<form name="frm_download_chart_data_{{chart_details.chartIndex}}" id="frm_download_chart_data_{{chart_details.chartIndex}}" method="post" action="<?php echo $this->url('chart/downloadxls');?>">
	<input type="hidden" id="frm_input_download_chart_codes_{{chart_details.chartIndex}}" name="chart_codes" value="">
	<input type="hidden" id="frm_input_download_chart_datatype_{{chart_details.chartIndex}}" name="chart_datatype" value="">
	</form>
	</div>
	<div class="h_graph_content_area" >
	<div class="" id="Jma_chart_container_{{chart_details.chartIndex}}">
	</div>
	</div>
	</div>
	</div>
	{{#if chart_details.isRightPannel}}
	<div class="col-lg-4 col-xs-12 pad0 h_graph_tab_area">
	<ul class="nav nav-tabs navtab-sm">
	<li class="active"><a data-toggle="tab" chart_index="{{chart_details.chartIndex}}" href="#Dv_dataseries_{{chart_details.chartIndex}}">Series</a></li>
	<li><a data-toggle="tab"  chart_index="{{chart_details.chartIndex}}"  href="#Dv_download_{{chart_details.chartIndex}}">Download</a></li>
	{{#unless mychart_details.isMyChart}}
	<li class="mdl Graph_tabset_tab_head_share"><a data-toggle="tab"  chart_index="{{chart_details.chartIndex}}"  href="#Dv_share_{{chart_details.chartIndex}}">Share</a></li>
	{{/unless}}
	</ul>
	<div class="tab-content">
	<div id="Dv_dataseries_{{chart_details.chartIndex}}" class="tab-pane fade in active">
	&nbsp;
	</div>
	<div id="Dv_download_{{chart_details.chartIndex}}" class="tab-pane fade">
	<div>
	<div><h5 class="addmore">Export Chart</h5></div>
	<div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr><td align="left">
	Export as
	</tr></td>
	<tr><td align="center">
	<select id="tab_export_chart_image_select_format_{{chart_details.chartIndex}}" class="addmore-select form-control">
	<option value="csv">Data (CSV)</option>
	<option value="jpeg">Image (JPEG)</option>
	<option value="png">Image (PNG)</option>
	<option value="pdf">Document (PDF)</option>
	<option value="ppt">PowerPoint (PPTX)</option>
	</select>
	</tr></td>
	<tr><td align="left" style="display:none">
	Select size
	</tr></td>
	<tr><td align="center" style="display:none">
	<select id="export_chart_image_size_{{chart_details.chartIndex}}" class="addmore-select form-control">
	<option value="small">Small</option>
	<option value="medium">Medium</option>
	<option value="large">Large</option>
	</select>
	</tr></td>
	<tr><td align="center">
	<input type="button" class="btn btn-primary btn-sm" value="Export" onClick="JMA.JMAChart.exportTabChart({{chart_details.chartIndex}});">&nbsp;&nbsp;
	<input type="button" class="btn btn-primary btn-sm" value="Print" onClick="JMA.JMAChart.printChart({{chart_details.chartIndex}});">
	</tr></td>
	</table>
	</div>
	</div>
	</div>
	<div id="Dv_share_{{chart_details.chartIndex}}" class="tab-pane fade" >
	<div class="hgta_socsha">
	<h5>Share on Social Media</h5>
	<ul class="list-inline">
	<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
	<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
	<li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
	<li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
	</ul>
	</div>
	<div>
	<div class="spacer10"></div>
	<div class="social_share_buttons">
	<h5 class="">Share Link</h5>
	<div class="social_share_button">
	<input type="text" class="graph_share_input form-control" name="graph_share_url_{{chart_details.chartIndex}}" id="graph_share_url_{{chart_details.chartIndex}}" value="<?php echo '//'.$_SERVER["SERVER_NAME"].$_SERVER['REQUEST_URI'];?>" onclick="this.select()" readonly>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	{{/if}}
	</div>
	</div>
	</script>
	<script type="template/alanee" id="template_graph_section_series">

	<div>
	<div class="graph-list" id="Dv_placeholder_graph_series_section_{{chartIndex}}">
	{{#each current_series}} 
	<div class="graph-line" id="Dv_placeholder_graph_currentseries_select_{{../chartIndex}}_{{@index}}">
	<div class="input-group">
	<div class="input-group-addon">
	<i class="fa fa-minus" onClick="JMA.JMAChart.SeriesColorDropdown({{../chartIndex}},{{@index}},this)" ></i>
	</div>
	<select class="form-control" onChange="JMA.JMAChart.populateYSubDropdown({{../chartIndex}},{{@index}},this)">
	{{#each series}}
	<option value="{{@index}}" {{#if isCurrent}}selected{{/if}}>{{label}}</option>
	{{/each}}
	</select>

	</div>
	

	{{#each series}}
	{{#if isCurrent}}
	<div class="Dv_placeholder_graph_currentseries_ysub_select">
	<select class="chart-select form-control" onChange="JMA.JMAChart.replaceThisGraphCode({{../../../chartIndex}},{{@../index}},this)">
	{{#each series}}
	<option value="{{code}}" {{#if isCurrent}}selected{{/if}}>{{label}}</option>
	{{/each}}
	</select>
	</div>
	{{/if}}
	{{/each}}
	<div class="graph-line-controls">{{#ifCond @index '>' 0}}<a href="javascript:void(0)" onClick="JMA.JMAChart.removeThisChartCodeByIndex({{../../chartIndex}},{{@index}})">Remove</a>{{/ifCond}}</div>
	</div>
	{{/each}}
	</div>
	{{#if isAddMoreseries}}
	<div class="graph-addmore">
	<span class="addmore marb20">Add More Series</span>
	<select id="select_series_addmore-select_{{chartIndex}}" class="addmore-select form-control">
	{{#each available_series}}
	<option value="{{code}}">{{label}}</option>
	{{/each}}
	</select>
	<div class="dv_addmore-button"><a class="addmore-button" href="javascript:void(0)" onClick="JMA.JMAChart.addThisGraphCode({{chartIndex}})">Add more</a></div>
	</div>
	{{/if}}
	{{#ifCond isBarChart '!=' true}}
	<div>
	<label class="control control--checkbox">Multiple yAxis
	<input type="checkbox" value="checkbox" name="multiaxis_checkbox__{{chartIndex}}" id="multiaxis_checkbox__{{chartIndex}}" {{#if isMultiAxis}}checked{{/if}} onClick="JMA.JMAChart.switchToMultiAxisLine({{chartIndex}},this);"/>
	<div class="control__indicator"></div>
	</label>
	</div>
	{{/ifCond}}
	<div>
	<label class="control control--checkbox">Bar chart
	<input type="checkbox" value="checkbox" name="barchart_checkbox__{{chartIndex}}" id="barchart_checkbox__{{chartIndex}}" {{#if isBarChart}}checked{{/if}} onClick="JMA.JMAChart.switchToBarChart({{chartIndex}},this);"/>
	<div class="control__indicator"></div>
	</label>
	</div>
	</div>
	</script>
	<!-- Template End -->
	<header>
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $this->url('/');?>">
						<img src="<?php echo $this->images;?>logo_b.png" alt="">
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="mainNav">
					<ul class="nav navbar-nav navbar-right">
						<li class="<?php echo ($this->controllername=='home')?'active':'';?>"><a href="<?php echo $this->url('/');?>" class="top_link_common">Home</a></li>
						<!--<li class="<?php echo ($this->controllername=='aboutus' && $this->actionname=='index')?'active':'';?>"><a href="<?php echo $this->url('aboutus');?>" class="top_link_common">About us</a></li>-->
						<li class="<?php echo ($this->controllername=='products')?'active':'';?>"><a href="<?php echo $this->url('products');?>" class="top_link_common">Products</a></li>
						<!-- <li><a href="<?php // echo $this->url('careers');?>"class="top_link_common">Careers</a></li> -->
						<li class="<?php echo ($this->controllername=='contact')?'active':'';?>"><a href="<?php echo $this->url('contact');?>" class="top_link_common">Contact</a></li>
						<li class="<?php echo ($this->actionname=='privacypolicy')?'active':'';?>"><a href="<?php echo $this->url('aboutus/privacypolicy');?>" class="top_link_common">Our Privacy Policy</a></li>
						<li class="<?php echo ($this->actionname=='commercial_policy')?'active':'';?>"><a href="<?php echo $this->url('aboutus/commercial_policy');?>" class="top_link_common">Commercial Policy </a></li>
						<?php if(isset($_SESSION['user']) && $_SESSION['user']['id'] > 0) {?>
						<li class="jma_username">
							<a href="<?php echo $this->url('user/myaccount');?>" class="top_link_common">
								<font color="red"><?php echo ucfirst($_SESSION['user']['fname']).' '.$_SESSION['user']['lname'];?></font>
							</a>
						</li>
						<li class="last"><a href="<?php echo $this->url('user/logout');?>" class="top_link_common">Signout</a></li>
						<?php } else {?>
						<li class="last" id="lnk_client_login"><a href="<?php echo $this->url('user/login');?>" class="top_link_client_login">USER LOGIN</a></li>
						<?php }?>
						<li>
							<div class="gte_con" id="google_translate_element"></div>
						</li>
					</ul>
					<?php  include ('view/templates/responsive_navigation.php');?>
					<!-- dropdown indide dropdown script -->
					<script type="text/javascript">
					$(document).ready(function(){
						$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
							event.preventDefault();
							event.stopPropagation();
							$(this).parent().siblings().removeClass('open');
							$(this).parent().toggleClass('open');
						});
						$('.navbar-toggle').on('click', function(event) {
							$('body').toggleClass('overflow-hidden');
							$('.myfol_toggle').toggleClass('hide');
						});
					});
					</script>
					<script type="text/javascript">
					function googleTranslateElementInit() {
						new google.translate.TranslateElement({pageLanguage: 'en',includedLanguages: 'af,ca,da,de,el,en,es,fr,it,ko,nl,pl,pt,ru,sv,tl,bn,gu,hi,kn,ml,mr,ne,pa,ta,te,ur', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
					}
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
	</header>
	<section>
		<div id="carousel_home" class="carousel slide carousel_home" data-ride="carousel">
			<?php if($this->controllername=='home'){ $Bancount=3;$smallBan_Name='slider'; ?> 
			<!-- Indicators -->
			<!-- <ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>-->
			<?php }else{ 
				$Bancount=1;
				$smallBan_Name='smallslider';
			}?>
			<!-- Wrapper for slides -->
			<div class="carousel-inner <?php echo $smallBan_Name;?>" role="listbox">
				<?php   for ($b=0; $b <$Bancount ; $b++) { ?>
				<div class="item <?php echo ($b==0)?"active":'';?>">
					<img src="images/slider/<?php echo $smallBan_Name.($b+1);?>.jpg" alt="JMA Banners">
					<div class="color_overlayd"></div>
					<div class="carousel-caption">
						<h4>CONCISE AND INSIGHTFUL ANALYSIS ON THE INDIAN ECONOMY</h4>
						<?php if(!isset($_SESSION['user'])){ ?>
						<a class="btn btn-primary btn-sm btn_carhom" target="_blank" href=<?php echo $this->url('user/login');?>>
							<span class="btn_carporate">
								<i class="glyphicon glyphicon-log-in"></i>Signin
							</span>
						</a>
						<?php } ?>
						<a class="btn btn-primary btn-sm btn_carhom" target="_blank" href=<?php echo $this->url('products/offerings');?>>
							<span class="btn_carporate">
								<i class="fa fa-hand-o-right"></i>What We Offer
							</span>
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php if($this->controllername=='home'){ ?> 
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel_home" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel_home" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
			<?php   } ?>
		</div>
		
	</section>
	<section class="container">
		<?php  include ('view/templates/left_navigation.php');?>
		<?php $this->view (); ?>
	</section>
	<?php include ('view/templates/footer.php');?>
	<!-- Login Modal Start -->
	<div class="modal fade" id="Dv_modal_login" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="pull-left">
						<h4 class="modal-title">Login</h4>
					</div>
					<!-- <ul class="list-inline list_sigmod">
						<li>
							<div class="download-img" >
								<i class="fa fa-user fa-lg color-green"></i><b>FREE</b>
							</div>
						</li>
						<li>
							<div class="premium-img color-blue icon-sup">
								<i class="fa fa-user fa-lg"></i>
								<sup>
									<i class="fa fa-star fa-fw"></i>
								</sup>
								<b>PREMIUM</b>
							</div>
						</li>
						<li>
							<div class="premium-img">
								<i class="fa fa-building fa-lg"
								style="color: #22558F;"></i><b>CORPORATE</b>
							</div>
						</li>
					</ul>-->
				</div>
				<div class="modal-body">
					<form name="login_frm_ajx" id="login_frm_ajx" class="signup_frm" action="<?php echo $this->url('/user/login');?>" method="post">
						<!--<div class="text-center form-group mychart">
							<p>This feature is restricted <b>for logged-in users only.</b> <br> If you
								are a FREE / PREMIUM / CORPORATE account user, please log-in.<br>
							</p>
						</div>-->
						<!--<div class="text-center form-group premium">
							<p>This content is restricted <b>for paying users only.</b> <br> If you
								are a PREMIUM / CORPORATE account user, please log-in.<br>
							</p>
						</div>-->
						<div class="text-center form-group download">
							<p>Please log-in to access our <b>data download function.</b><br></p>
						</div>
						<div class="text-center form-group">
							<a href="<?php echo $this->url('user/linkedinProcess');?>" class="linkedIn">
								<img src="<?php echo $this->images;?>sign-in-with-linkedin.png" />
							</a>
						</div>
						<div class="sinup_orcon"><p>OR</p></div>
						<p class="login_frm_ajx_login_status text-center text-danger" style="font-size: 12px;display: none;"></p>
						<div class="form-group col-xs-12 col-sm-6 sm-pad padl0">
							<!-- <label for="login_email" class="control-label visible-xs">&nbsp;</label>  -->
							<input class="form-control" placeholder="Email" name="login_email" id="login_email">
						</div>
						<div class="form-group col-xs-12 col-sm-6 sm-pad padr0">
							<!-- <label for="login_password" class="control-label visible-xs">&nbsp;</label> -->
							<input type="password" class="form-control"  placeholder="Password" name="login_password" id="login_password"  />
							<input type="hidden" name="chart_login_perm_type" id="chart_login_perm_type" >
							<input type="hidden" name="chart_login_chart_index" id="chart_login_chart_index">
							<input type="hidden" name="chart_login_premium_url" id="chart_login_premium_url">
						</div>
						<div class="full-width text-center marb20">
							<button type="button"  class="btn btn-primary btn-sm" name="login_btn" id="pop_login_btn">
								Submit
							</button>
							<a class="btn" id="SubmitForgotPss" href="<?php echo $this->url('user/forgotpassword');?>">
								Forgot Password?
							</a>
						</div>
						<p class="premium-logininfo">
							<a href="<?php echo $this->url('products');?>">Upgrade or Register for </a> PREMIUM or CORPORATE account.
						</p>
						<p class="download-logininfo">
							Not registered?<br/>
							<i class="fa fa-play-circle" style="color: #F39019; font-size: 20px;padding: 1px 5px 3px 2px;"></i>
							<a href="<?php echo $this->url('products');?>">Setup a <b>Free Account</b></a> to access our services free of charge.
						</p>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Login Modal End-->
	<!-- Auto Login  Modal Start-->
	<?php if(!isset($_SESSION['user'])){?>
	<div class='popup'>
		<div class='cnt223'>
			<div class="POPUser">
				<i class="fa fa-user fa-lg"></i>User Login
				<div alt='quit' class='x' id='x'>
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div id="Dv_login_wrapper">
				<form name="login_frm" id="login_frm"
				action="<?php echo $this->url('/user/login');?>" method="post">
				<div class="login_box_input">
					<input type="text" placeholder="Email"
					class="formPop_textfield" name="login_email"
					id="login_email" />
				</div>
				<div class="login_box_input">
					<input type="password" placeholder="password"
					class="formPop_textfield" name="login_password"
					id="login_password" />
				</div>
				<div class="full-widthf">
					<label class="control control--checkbox">Keep me signed in
						<input type="checkbox" value="y" name="login_rememberMe" id="login_rememberMe"/>
						<div class="control__indicator"></div>
					</label>
				</div>
				<div class="full-widthf text-center">
					<input type="submit" value="Submit" class="btn btn-primary btn-sm" name="login_btn"  />
				</div>
				<div class="ForPassword">
					<a href="<?php echo $this->url('user/forgotpassword');?>">Forgot
						your password?</a>
					</div>
					<div class="sinup_orcon"><p>OR</p></div>
					<div class="chat_signlin">
						<a href="<?php echo $this->url('user/linkedinProcess');?>">
							<img src="<?php echo $this->images;?>sign-in-with-linkedin.png" />
						</a>
					</div>
				</form>
			</div>
		</div>
		
	</div>
	<div class='popupDev'>
	 <div class='cnt223Dev' align="center">
			<div class="POPUser">
				<i class="glyphicon glyphicon-alert"></i>Alert !
				<div alt='quit' class='xDev' id='x'>
					<i class="fa fa-times" style="color: #EA2635"></i>
				</div>
			</div>
			<div style="padding:5 25px;"><h4>This site is under development. The information provided in the website may not be accurate.</h4></div>
		</div>
	</div>	
	<?php }?>
	<!-- Auto Login Modal End-->
	<!-- Upgrade Premium Feature Modal Start-->
	<!-- <div class="modal fade" id="Dv_modal_upgrade_premium_feature" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-warning">Sorry..! This feature restricted</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<p class="text-center"><i class="fa fa-warning" style="font-size: 6em;line-height:2em;color: #FFA500;"></i></p>
						</div>
						<div class="col-xs-12 col-sm-9">
							<p><b> Sorry..! With a free account, you can save & edit up to 4 charts & tables in your personal online folder. If you wish to save more than 5 charts, please upgrade to a PREMIUM or CORPORATE account. Otherwise, you can also delete unnecessary charts & tables from your folder. For more details, please check your account details <a href="<?php echo $this->url('user/myaccount/subscription');?>">here</a></b>. </p>
							<p> Thank you again for being a JMA user and we welcome any feedback you like to share with us at <a href="mailto:info@japanmacroadvisors.com">info@japanmacroadvisors.com</a>. </p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div> -->
	<!-- Upgrade Premium Feature End-->
	<!-- Upgrade Premium content Modal Start-->
	<!-- <div class="modal fade" id="Dv_modal_upgrade_premium_content" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-warning">Sorry..! This feature restricted</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<p class="text-center">
								<i class="fa fa-warning" style="font-size: 6em;line-height:1.5em;color: #FFA500;"></i>
							</p>
						</div>
						<div class="col-xs-12 col-sm-9">
							<p>
								<b> 
									Sorry..! you do not have permission to view premium ontents. Please review your subscription status 
									<a href="<?php echo $this->url('user/myaccount');?>">Account details</a> . 
								</b>
								<br><br> Thank you again for being a JMA user and we welcome any feedback you like to share with us at 
								<a href="mailto:info@japanmacroadvisors.com">info@japanmacroadvisors.com</a>. 
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div> -->
	<!-- Upgrade Premium content End-->
	<!-- Createfolder Restricted Modal Start-->
	<div class="modal fade" id="Dv_modal_createfolder_restricted" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-warning">Sorry..! Folder Creation - Reached limit</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<p class="text-center"><i class="fa fa-warning" style="font-size: 6em;color: #FFA500;"></i></p>
						</div>
						<div class="col-xs-12 col-sm-9 sm-txtcen">
							<br>
							<p><b>Sorry..! you have reached maximum allowed folders.<br>You not allowed to create more folders.</b>
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Createfolder Restricted End-->
	<!-- Content - Reached limit Modal Start-->
	<div class="modal fade" id="Dv_modal_addchart_restricted" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-warning">Sorry..! Add Content - Reached limit</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<p class="text-center"><i class="fa fa-warning" style="font-size: 6em;color: #FFA500;"></i></p>
						</div>
						<div class="col-xs-12 col-sm-9 sm-txtcen">
							<br>
							<p><b>Sorry..! you have reached maximum allowed content for this folder.You not allowed to add more content.</b>
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Content - Reached limit End-->
	<!-- Common Error Start-->
	<div class="modal fade" id="Dv_modal_error_common" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-danger">Error...!</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<p class="text-center"><i class="fa fa fa-times-circle" style="font-size: 6em;line-height:1.5em;color:red;"></i>
							</p>
						</div>
						<div class="col-xs-12 col-sm-9 sm-txtcen">
							<p>
								Sorry..! Something went wrong while displaying this webpage.<br>To continue, please click on "Refresh" or wait for the page to get refreshed.
								<br><br> <b>Page will refresh in <span id="error_page_refresh_countdown" style="font-weight: bold;"></span> seconds.</b>								
							</p>
							<button class="btn btn-primary btn-sm" id="common_btn_refresh" type="button" onclick="location.reload();"><i class="fa fa-refresh"></i> Refresh</button>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Common Error End-->
	<!-- Common Error with message Start-->
	<div class="modal fade" id="Dv_modal_error_common_with_message" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-danger">Error...!</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12 col-sm-3">
							<p class="text-center"><i class="fa fa fa-times-circle" style="font-size: 6em;color:#E60013;"></i></p>
						</div>
						<div class="col-xs-12 col-sm-9 sm-txtcen">
							<br>
							<p><b>Sorry..! Something went wrong and an error occured.
								<br><br><span id="error_page_error_message"></span></b>
							</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Common Error with message End-->

	<!-- Mycyhart Floating Menu-->
	<div class="myfol_toggle">
		<div class="mft_ttl"> <i class="fa fa-angle-right"></i></span></div>
		<div class="mtf_content">
			<ul class="menu top notranslate list-unstyled">
				<li class="sub-menu folders mychart-menu-set">
					<ul class="list-unstyled">
						<?php if(!isset($_SESSION['user'])){ ?>
						<li class="mycha_toglogin">
							Please login to access MyChart function
						</li>
						<?php }else{
							echo $this->resultSet['result']['category']['folders'];
						} ?>

						<li class="add-folder">
						  							
							<?php if(!isset($_SESSION['user'])){ ?>
							<a class="btn btn-primary mtfc_btn">
								<i class="fa fa-sign-in"></i> Login
							</a>
							<?php }else{ ?>
							<a class="btn btn-primary mtfc_btn" data-toggle="modal" data-target="#modaladd_folder">
								<i class="fa fa-plus-square"></i> Add Folder
							</a>
							<?php } ?>
						</li>
					</ul>
				</li>
			</ul>
			

		</div>
	</div>
	<!-- Mycyhart Floating Menu-->
	<!-- add folder modal -->
	 <div class="modal fade" id="modaladd_folder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	   <div class="modal-content">
		<div class="modal-header">
		 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		 <h4 class="modal-title" id="myModalLabel">Add Folder</h4>
		</div>
		<form name="frmEditFolder" id ="frmEditFolder" action="" method="post" >
		<div class="modal-body">
		 <div class="full-width">
		   <div class="form-group">
			<label for="exampleInputEmail1">Folder Name</label>
			<input type="text" class="form-control" id="editfolderName" name="editfolderName" placeholder="Write your folder name here">
		   <div id="errFolderName" style="color:red;"></div>
		   </div>
		 </div>
		</div>
		<div class="modal-footer">
		 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		 <button type="button" id="saveFolderName" class="btn btn-primary">Save</button>
		</div>
		</form>
	   </div>
	  </div>
	 </div>

</body>
<!-- Go to www.addthis.com/dashboard to customize your tools -->


<script type="text/javascript">


$('.mft_ttl').on('click', function(e){
	e.preventDefault();
	$('.myfol_toggle .mtf_content').toggleClass('active');
	$(this).toggleClass('active');
});
</script>
<script type="text/javascript">
//script
if ( !("placeholder" in document.createElement("input")) ) {
	$("input[placeholder], textarea[placeholder]").each(function() {
		var val = $(this).attr("placeholder");
		if ( this.value == "" ) {
			this.value = val;
		}
		$(this).focus(function() {
			if ( this.value == val ) {
				this.value = "";
			}
		}).blur(function() {
			if ( $.trim(this.value) == "" ) {
				this.value = val;
			}
		});
	});
  // Clear default placeholder values on form submit
  $('form').submit(function() {
  	$(this).find("input[placeholder], textarea[placeholder]").each(function() {
  		if ( this.value == $(this).attr("placeholder") ) {
  			this.value = "";
  		}
  	});
  });
}
$(document).ready(function(){
	$(window).scroll(function(){
		if ($(this).scrollTop() > 70) {
			$('nav').addClass('navbar-fixed-top');
		} else {
			$('nav').removeClass('navbar-fixed-top');
			
		}
	});
	$(document).on('click','li.nav_edittabs',function(){

		$(this).parents("div").siblings(".h_graph_tab_area").slideToggle();


		
	});
})
</script>
</html>