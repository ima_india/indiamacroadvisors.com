<!--footer section-->
<footer>
  <div class="footer_container">
    <div class="container">
      <!---<div class="col-md-4 col-xs-12 pad0">
        <div class="col-md-12 col-sm-6 col-xs-12">
          <div class="sub-title">
            <h5>Popular Search</h5>
            <div class="sttl-line"></div>
          </div>
          <ul class="list-unstyled list-fontawesome list_popchart">
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Job Offers to Applicant Ratio</a>
            </li>
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Unemployment Rate</a>
            </li>
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Balance of Payment</a>
            </li>
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Exchange Rates</a>
            </li>
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Cabinet Approval Rating</a>
            </li>
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Presentation Materials</a>
            </li>
            <li>
              <i class="fa fa-hand-o-right" aria-hidden="true"></i>
              <a href="">Industrial Production</a>
            </li>
          </ul>
        </div>
      </div>--->
      <div class="col-xs-12 col-md-8" style="width:100%;">
        <div class="sub-title">
          <h5>About us</h5>
          <div class="sttl-line"></div>
        </div>
        <p> India Macro Advisors Inc. ("We") are not an investment advisory and we do not make any offer or solicitation to buy/sell financial securities or other type of assets. The information contained herein has been obtained from, or is based upon, sources believed by us to be reliable, but we do not make any representation or warranty for their accuracy or completeness. The text, numerical data, charts and other graphical contents we provide ("IMA contents") are copyrighted properties of Japan Macro Advisors ("Us"). While we permit personal non-commercial usage of IMA contents, it should be accompanied by an explicit mention that the contents belong to us. We do not allow any reproduction of IMA contents for other purposes unless specifically authorised by us. Please contact <a href="mailto:info@japanmacroadvisors.com">info@indiamacroadvisors.com</a> to seek our authorization. </p>
      </div>
    </div>
  </div>
  <div class="footer_copycont">
    <div class="container">
      <div class="col-xs-12 col-md-4">
        <p class="copy_right">Copyright &copy; 2012-<?php echo date('Y');?>; <a href="">INDIA MACRO ADVISORS</a></p>
      </div>
      <div class="col-xs-12 col-md-8">
        <ul class="list-inline pull-right">
          <li><a href="<?php echo $this->url('/');?>">Home</a> |</li>
          <li><a href="<?php echo $this->url('user/newsletters');?>">Newsletter</a> |</li>
          <li><a href="<?php echo $this->url('aboutus/termsofuse');?>">Terms Of Use </a> |</li>
          <li><a href="<?php echo $this->url('aboutus/privacypolicy');?>">Our Privacy Policy </a> |</li>
          <li><a href="<?php echo $this->url('aboutus/commercial_policy');?>">Commercial Policy </a> |</li>
          <li><a href="<?php echo $this->url('contact');?>">Contact</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>


<!--footer section-->
<script type="text/javascript" src="<?php  echo $this->assets."plugins/readmore/readmore.js";?>"></script>
<script type="text/javascript">
$('iframe').load(function(){
var ht=$(this).contents().height(); //alert($('iframe').contents().height() + 'is the height');
//document.getElementById('frame').height= (ht) + "px";
this.style.height = (ht) + "px";
//if(ht>495)
//{
//$(this).css("height",ht);
//}
});
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36471452-1']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>