<?php
$briefse = $this->resultSet['result']['briefseries'];
?>
<div class="col-xs-12 col-md-10">
  <div class="main-title">
    <h4>JMA brief series</h4>
    <div class="mttl-line"></div>
  </div>
  
  
           <?php if(count($briefse)>0){ foreach ($briefse as $briefSeriesRlts) {	
	        $title = $briefSeriesRlts['briefseries_title'];
			$title_image = $briefSeriesRlts['briefseries_title_img'];
			$briefseries_summery_path = $briefSeriesRlts['briefseries_summary_path'];
			$briefseries_ppt_path = $briefSeriesRlts['briefseries_ppt_path'];
			$date =  $briefSeriesRlts['briefseries_date'];
			$premium =  $briefSeriesRlts['is_premium'];
			$id = $briefSeriesRlts['briefseries_id']; ?>

				  <div class="jmabrief_container">
					<div class="row">
					  <div class="col-sm-4">
						<img src="<?php echo $title_image;?>" alt="" />
					  </div>
					  <div class="col-sm-8">
						<a class="btn btn-secondry" href="">Download</a>
						<!-- <p><img src="<?php echo $this->images;?>breif_images/download.png" alt="download" /></p> -->
						<h5><?php echo $title; ?></h5>
						<ul class="list-unstyled">
						  <li>
							
							 <?php if($briefseries_summery_path !="") { ?>
							   <i class="fa fa-file-pdf-o" ></i>
							       
									<a target="_blank" href="<?php echo $this->url('/').$briefseries_summery_path; ?>">Summary</a><br>
								  
								   
							 <?php } ?>
						  </li>
						  <li>
							
							<?php if($briefseries_ppt_path !="") { ?>
							  <i class="fa fa-file-powerpoint-o" ></i>
							    
								     <a target="_blank" href="<?php echo $this->url('/').$briefseries_ppt_path; ?>">Presentation slide</a><br>
								
							<?php } ?>
						  </li>
						</ul>
					  </div>
					</div>
				  </div>
  
        <?php } } else { ?>	 
  
  
  
<div class="jmabrief_container">
    <div class="row" >
    
      <div class="col-sm-12 text-center text-danger">
      
        <h5>Contents Not Found</h5>
        
      </div>
    </div>
  </div>
 
 <?php } ?>
  
  
  
</div>
