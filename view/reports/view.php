<div class="col-xs-12 col-md-10">
	<?php
//exit($this->resultSet['status']."sdfsfsfsafasdf");
	if($this->resultSet['status'] != 1) {
		?>
		<h3>Sorry, <?php echo $this->resultSet['message'];?></h3>
		<?php
	} else {
		$resn = $this->resultSet['result']['posts'];
		?>
		<?php
		$count = count($resn);
		if($count==0)
		{
			?>
			<h3>Sorry, No news found</h3>
			<?php
		} else {
			?>
			<div itemscope itemtype="http://schema.org/Article">
			<div class="sec-date main-title">
				<?php if(!empty($resn[0]['post_released'])) {?><span class="released"><?php echo stripslashes($resn[0]['post_released']);?></span><?php }?>
				<h4 itemprop="name" class="<?php if(empty($resn[0]['post_released'])) echo "title-only"; ?>"><?php echo stripslashes($resn[0]['post_title']);?></h4>
				<div class="mttl-line"></div>
				</div>
				<?php if(!empty($resn[0]['post_heading'])) {?><h5 style="margin:0px"><?php echo stripslashes($resn[0]['post_heading']);?></h5><?php }?>
				<?php if(!empty($resn[0]['post_subheading'])) {?><h3><?php echo stripslashes($resn[0]['post_subheading']);?></h3><?php }?>
				<div itemprop="articleBody">
					<?php echo $resn[0]['post_cms']; // makeChart(cleanMyCkEditor($resn[0]['post_cms']));?>
				</div>
				<div class="addthis_sharing_toolbox" style="float: right"></div>
				<meta itemprop="articleSection" content="Japan Economy">
				<meta itemprop="url" content="http:/<?php echo $_SERVER['REQUEST_URI'];?>">
				<span itemprop="author" itemscope itemtype="http://schema.org/Person">
					<meta itemprop="datePublished" content="<?php echo date(strtotime($resn['post_datetime']));?>">
					<meta itemprop="name" content="Takuji Okubo">
				</span>
				<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
					<meta itemprop="name" content="Japanmacroadvisors">
				</span>
			</div>
			<?php
		}
		?>
	</div>
	<?php
 // include('view/templates/rightside.php');
	?>
	<script type="text/javascript">
	$(document).ready(function(e) {
		<?php $cat_array_og = json_encode($this->resultSet['result']['category_array']); ?>
		var category_array = JSON.parse('<?php echo $cat_array_og;?>');
		var cat_selected = '';
		$.each(category_array,function(catId,row){
			cat_selected = '.left_cat_'+catId;
			$(cat_selected).addClass("minus");
			$(cat_selected).parent('ul').css('display','block');
		});
		$(cat_selected).removeClass('minus');
		$(cat_selected).addClass('active');
	});
	</script>
	<?php
}
?>