<?php
//echo '<pre>';
//print_r($this->resultSet['result']['category']['nonpremium']);
//strtolower($str)
?>
<div class="col-md-10 col-xs-12 conpage_container">
  <div class="main-title">
    <h4>Contact Us</h4>
    <div class="mttl-line"></div>
  </div>
  <h4>General inquiry</h4>
  <p>
    For general inquiry, please send e-mail to 
    <a href="mailto:info@indiamacroadvisors.com">info@indiamacroadvisors.com</a>
  </p>
  <p>
    For technical assistance and subscription inquiry, please send e-mail to 
    <a href="mailto:support@indiamacroadvisors.com">support@indiamacroadvisors.com</a>
  </p>
  <ul class="nav nav-tabs">
    <li class="active">
      <a role="tab" data-toggle="tab" href="#t1">To reach us</a>
    </li>
    <li>
      <a role="tab" data-toggle="tab" href="#t2">Our mailing address</a>
    </li>
  </ul>
  <div class="tab-content">
    <div role="tabpanel" id="t1" class="tab-pane fade in active">
      <div class="sub-title">
        <h5>To visit India Macro Advisors</h5>
        <div class="sttl-line"></div>
      </div>
      <p class="marb20">We are located inside the Roppongi Library Office on the 49th floor of Roppongi Hills, Mori Tower. We would advise our visitors to kindly ask ground floor receptionists to direct you to the elevator bank that would take you to the 49th floor and then ask for us at the Library reception.</p>
      <div class="sub-title">
        <h5>To reach Roppongi Hills</h5>
        <div class="sttl-line"></div>
      </div>
      <div class="panel-group conpag_colpan" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Subway
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <table class="table table-bordered marb0">
                <tbody>
                  <tr>
                    <th>Hibiya Line</th>
                    <td>Roppongi Station</td>
                    <td>Exit 1C</td>
                    <td>3 min.</td>
                  </tr>
                  <tr>
                    <th>Oedo Line</th>
                    <td>Roppongi Station</td>
                    <td>Exit 3</td>
                    <td>6 min.</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="panel panel-default cpc_bus">
          <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Bus
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <p>
                <b>From Shibuya Station: </b><br/>
                Toei #01 or RH #01 bus to Roppongi Hills<br>
                (Shibuya - Roppongi Hills)
              </p>
              <p>
                <b>From Shimbashi Station: </b><br />
                Toei #01 bus to Roppongi Roku-chome <br>
                (Shimbashi - Shibuya)
              </p>
            </div>
          </div>
        </div>
        <div class="panel panel-default cpc_car">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                By Car
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>
                Via Shuto Expressway's Iikura and Kasumigaseki Exits 10 min.<br>
                Haneda Airport 40 min.
              </p>
              <h5>From Tokyo Station</h5>
              <p>
                Take the Marunouchi Line to Ginza Station and transfer to the Hibiya Line for <br>
                Roppongi Station (30 min.)
              </p>
              <h5>From Haneda Airport</h5>
              <p>
                Take the Tokyo Monorail to Hamamatsu-cho Station and then take a taxi (40 min.)<br>
                or<br>
                Take the Keikyu Line to Daimon Station and transfer to the Oedo Line for Roppongi <br>
                Station (50 min.)
              </p>
              <h5>From Narita Airport</h5>
              <p>
                Airport Limousine Bus for Grand Hyatt Tokyo at Roppongi Hills (90 min.)<br>
                or<br>
                Take the Narita Express to Tokyo Station and then take a taxi or the subway (70 min.)
              </p>
            </div>
          </div>
        </div>
      </div>      
    </div>
    <div role="tabpanel" id="t2" class="tab-pane fade cp_addmap">
      <p><strong>Address:</strong></p>
      <p>
        No.15. SLV complex,<br />
        2nd Floor, 19th Main Road,<br />
		Koramangala layout,6th Block,<br />
		Bangalore- 560095<br />
        
      </p>
	  <iframe height="500" frameborder="0" scrolling="no"marginheight="0" marginwidth="0"  src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3349.91053609751!2d77.62322000000002!3d12.9418055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sNo.15.+SLV+complex%2C2nd+Floor%2C+19th+Main+Road%2CKoramangala+layout%2C6th+Block%2CBangalore-+560095!5e1!3m2!1sen!2sjp!4v1478253548387"></iframe>
	  
	  
     
    </div>
  </div>
</div>
<?php
 //include('view/templates/rightside.php');
?>