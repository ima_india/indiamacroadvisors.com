 <div class="col-md-7 col-xs-12">
  <div class="row">
    <div class="col-xs-12">
      <div class="main-title">
        <h4> Notification based on the Specified Commercial Transactions Act</h4>
        <div class="mttl-line"></div>
      </div>
      <table class="table table-bordered commtbl" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td>Legal Name</td>
            <td>India Macro Advisors Inc.</td>
          </tr>
          <tr>
            <td>Address</td>
            <td>
              No.15. SLV complex, 2nd Floor, 19th Main Road, Koramangala layout, 6th Block, Bangalore- 560095
            </td>
          </tr>
          <tr>
            <td>Phone number</td>
            <td>03-5786-3275</td>
          </tr>
          <tr>
            <td>Email address</td>
            <td><a href="mailto:support@indiamacroadvisors.com">support@indiamacroadvisors.com</a></td>
          </tr>
          <tr>
            <td>Business hours</td>
            <td>10:00 ～ 17:00
              <br>Closed on Saturday, Sunday, National holidays
            </td>
          </tr>
          <tr>
            <td>Director of operations</td>
            <td>Takuji Okubo</td>
          </tr>
          <!--<tr>
            <td>Additional fees</td>
            <td>Not applicable</td>
          </tr>
          <tr>
            <td>Exchanges & Returns</td>
            <td>Due to the nature of digital subscription service, no amendments, cancellations, or refunds will be possible after purchase of the service. Your understanding is appreciated.</td>
          </tr>
          <tr>
            <td>Delivery times</td>
            <td>Immediate after the completion of the payment</td>
          </tr>
          <tr>
            <td>Accepted payment methods</td>
            <td>See our <a href="<?php echo $this->url('products');?>">product</a> page</td>
          </tr>
          <tr>
            <td>Payment period</td>
            <td>Credit card payments are processed immediately. </td>
          </tr>
          <tr>
            <td>Payment</td>
            <td>In advance</td>
          </tr>
          <tr>
            <td>Value</td>
            <td>¥3,500</td>
          </tr>--->
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php
include('view/templates/rightside.php');
?>