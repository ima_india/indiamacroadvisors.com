<?php
if ( ! defined('SERVER_ROOT')) exit('No direct script access allowed');
class Navigation extends AlaneeController {
	
	



	public function createLeftNavigation($nValues,$cat_url_param='',$pIsPremium = false) {
		$acl = new Acl();
		$out = '';
		if(is_array($nValues)) {
			$category_group = '';
			$cat_group_li = '';
			foreach ($nValues as $cat_id=>$cat_row) {
				if($category_group != $cat_row['details']['category_group'] && $cat_row['details']['category_group'] != ''){
					$first = true;
					$category_group = $cat_row['details']['category_group'];
					switch ($cat_row['details']['category_group']){
						case 'DATA & EVENTS':
						$cat_group_li = '<li class="menu_group_title "><i class="fa fa-bar-chart"></i>&nbsp;'.$cat_row['details']['category_group'].'</li>';
						break;
						case 'JMA REPORTS':
						$cat_group_li = '<li class="menu_group_title "><i class="fa fa-files-o"></i>&nbsp;'.$cat_row['details']['category_group'].'</li>';
						break;
						case 'JMA PREMIUM':
						$cat_group_li = '<li class="menu_group_title"><i class="fa fa-key"></i>&nbsp;'.$cat_row['details']['category_group'].'</li>';
						break;
					}
					
				}else{
					$cat_group_li = '';
					$first = false;
				}
				
				$premium_cat_lnk_cls = '';
				if($pIsPremium == false) {
					$isPremium = $cat_row['details']['premium_category'] == 'Y' ? true : false;
				}else{
					$isPremium = $pIsPremium;
				}
				$cat_url = $cat_url_param.$cat_row['details']['category_url']."/";
				if($cat_row['details']['new_icon_display'] == 'Y') {
					$new_icn_cd = '<span class="sublef_menunew">New</span>';
				} else {
					$new_icn_cd = '';
				}
				switch ($cat_row['details']['category_type']) {
					case 'P':
					$link_url = $this->url('page');
					break;
					case 'N':
					$link_url = $this->url('news');
					break;
					case 'M':
					$link_url = $this->url('materials');
					break;
					case 'L':
					$link_url = $this->url($cat_row['details']['category_link']);
					break;
					default:
					$link_url = $this->url('page');
				}
				$classes = "left_cat_$cat_id";
				$ids = "left_cat_$cat_id";
				if($isPremium == true) {
					$premium_uurl = $cat_row['details']['category_type'] =='L' ? $link_url : $link_url."/category/".$cat_url;
					if($this->isUserLoggedIn()==true) {
						// check permission
						if($acl->isPermitted('content', 'report', 'premiumaccess')==true){
							$link_url.= $cat_row['details']['category_type'] !='L' ? "/category/".$cat_url : '';
						} else {
							// show upgrade box
							$premium_cat_lnk_cls = 'lnk_inactive';
							$link_url='javascript:JMA.User.showUpgradeBox("premium","'.$premium_uurl.'")';
							$classes.= ' menu_premium_locked';
						}
					}else{
						// Show login window
						$premium_cat_lnk_cls = 'lnk_inactive';
						$link_url='javascript:JMA.User.showLoginBox("premium","'.$premium_uurl.'")';
						$classes.= ' menu_premium_locked';
					}
				} else {
					$link_url.="/category/".$cat_url;
				}
				if(count($cat_row['subcategories']) == 0) {
					$classes.=" noparent";
				} 
				if($cat_row['details']['post_category_parent_id'] == 0 && $first ==true){
					$classes.= ' menu_cat_group_first_entry';
				}



                                             

				if(count($cat_row['subcategories'])>0) {
					$out.=$cat_group_li."<li class='list-toggle submenu_leftside list-group-item'><a class='content_leftside_parent' data-toggle='collapse'  href='#$ids'>".$new_icn_cd.stripslashes($cat_row['details']['post_category_name'])."<i></i></a>";
					$out.="<ul id='$ids' class='collapse  list-group'>".$this->createLeftNavigation($cat_row['subcategories'],$cat_url)."</ul>";

				} else {
					$out.=$cat_group_li."<li class='$classes submenu_leftside list-group-item'><a  href='$link_url' class='$premium_cat_lnk_cls content_leftside_parent'>".$new_icn_cd.stripslashes($cat_row['details']['post_category_name'])."</a></li>";
				}
			}
		} 
		return $out;
	}
	
	public function getCategoryarrayFromUrlArray($urlArray) {
		$cat_array = array();
		if(is_array($urlArray)) {
			$parent_cat_id = 0;
			$postCategory = new postCategory();
			foreach ($urlArray as $rw_url) {
				if($rw_url != '') {
					$category_details = $postCategory->getThisCategoryDetailsByKeyAndParent(md5($rw_url),$parent_cat_id);
					$cat_array[$category_details['post_category_id']] = $category_details;
					$parent_cat_id = $category_details['post_category_id'];
				}
			}
			
		}
		return $cat_array;
	}
	
	public function getCategotyArrayParsedIntoPath($cat_array) {
		$response = '';
		if(is_array($cat_array)) {
			foreach ($cat_array as $rw_cat) {
				$response.=$rw_cat['category_url'].'/';
			}
		}
		return $response;
	}
	
	public function isThisCategoryPremium($cat_id){
		$postCategory = new postCategory();
		$cat_details = $postCategory->getThisCategoryById($cat_id);
		if($cat_details['premium_category'] == 'Y') {
			return true;
		}else if($cat_details['post_category_parent_id'] != 0) {
			return $this->isThisCategoryPremium($cat_details['post_category_parent_id']);
		}else {
			return false;
		}
	}

	public function createFolderNav($folders,$controllerName,$actionName) { 
		$result = '';
		if(is_array($folders)) {
			foreach ($folders as $idx=>$folder ) {
				$fid = $folder['folder_id'];
				$fname = $folder['folder_name'];
				if($idx < $_SESSION['user']['user_permissions']['mychart']['totalFolders']) {
					if($idx ==0){
						$result .= "<li class='folder'><a href='".$this->url('mycharts')."/#{$fid}'><i class='fa fa-folder'></i> <span data-id='{$fid}' data-folderName='{$fname}' contentEditable='false' class='folder-span-name'>{$fname}</span></a></li>";
					}else{
						$result .= "<li class='folder'><a href='".$this->url('mycharts')."/#{$fid}'><i class='fa fa-folder'></i> <span data-id='{$fid}' data-folderName='{$fname}' contentEditable='false' class='folder-span-name'>{$fname}</span></a><span class='del'><i class='fa fa-trash'></i></span></li>";
					}
				}else{
					$result .= "<li class='folder lnk_inactive'><i class='fa fa-folder'></i> <span data-id='{$fid}' data-folderName='{$fname}' contentEditable='false' class='folder-span-name'>{$fname}</span><span class='del'><i class='fa fa-trash'></i></span></li>";
				}
			}
		}

		return $result;
	} 

### Veera Start ##


	public function createResponsiveNavigation($nValues,$cat_url_param='',$pIsPremium = false) {
		$acl = new Acl();
		$out = '';
		if(is_array($nValues)) {
			$category_group = '';
			$cat_group_li = '';
			foreach ($nValues as $cat_id=>$cat_row) {
				if($category_group != $cat_row['details']['category_group'] && $cat_row['details']['category_group'] != ''){
					$first = true;
					$category_group = $cat_row['details']['category_group'];
					switch ($cat_row['details']['category_group']){
						case 'DATA & EVENTS':
						$cat_group_li = '<li><a href="javascript:;" class="mob_maitit"><i class="fa fa-bar-chart"></i>'.$cat_row['details']['category_group'].'</a></li>';
						break;
						case 'JMA REPORTS':
						$cat_group_li = '<li><a href="javascript:;" class="mob_maitit"><i class="fa fa-files-o"></i>'.$cat_row['details']['category_group'].'</a></li>';
						break;
						case 'JMA PREMIUM':
						$cat_group_li = '<li><a href="javascript:;" class="mob_maitit"><i class="fa fa-key"></i>'.$cat_row['details']['category_group'].'</a></li>';
						break;
					}
					
				}else{
					$cat_group_li = '';
					$first = false;
				}
				
				$premium_cat_lnk_cls = '';
				if($pIsPremium == false) {
					$isPremium = $cat_row['details']['premium_category'] == 'Y' ? true : false;
				}else{
					$isPremium = $pIsPremium;
				}
				$cat_url = $cat_url_param.$cat_row['details']['category_url']."/";
				if($cat_row['details']['new_icon_display'] == 'Y') {
					$new_icn_cd = '<span class="sublef_menunew">New</span>';
				} else {
					$new_icn_cd = '';
				}
				switch ($cat_row['details']['category_type']) {
					case 'P':
					$link_url = $this->url('page');
					break;
					case 'N':
					$link_url = $this->url('news');
					break;
					case 'M':
					$link_url = $this->url('materials');
					break;
					case 'L':
					$link_url = $this->url($cat_row['details']['category_link']);
					break;
					default:
					$link_url = $this->url('page');
				}
				$classes ="";
				if($isPremium == true) {
					$premium_uurl = $cat_row['details']['category_type'] =='L' ? $link_url : $link_url."/category/".$cat_url;
					if($this->isUserLoggedIn()==true) {
						// check permission
						if($acl->isPermitted('content', 'report', 'premiumaccess')==true){
							$link_url.= $cat_row['details']['category_type'] !='L' ? "/category/".$cat_url : '';
						} else {
							// show upgrade box
							$premium_cat_lnk_cls = 'lnk_inactive';
							$link_url='javascript:JMA.User.showUpgradeBox("premium","'.$premium_uurl.'")';
							$classes.= ' menu_premium_locked';
						}
					}else{
						// Show login window
						$premium_cat_lnk_cls = 'lnk_inactive';
						$link_url='javascript:JMA.User.showLoginBox("premium","'.$premium_uurl.'")';
						$classes.= ' menu_premium_locked';
					}
				} else {
					$link_url.="/category/".$cat_url;
				}
				if(count($cat_row['subcategories']) == 0) {
					$classes.=" noparent";
				} 
				if($cat_row['details']['post_category_parent_id'] == 0 && $first ==true){
					$classes.= ' menu_cat_group_first_entry';
				}
				if(count($cat_row['subcategories'])>0) {
					$out.=$cat_group_li."<li class='$classes dropdown'><a href='javascript:;' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>".stripslashes($cat_row['details']['post_category_name'])."<span class='caret'></span></a>";
					$out.="<ul class='menu sub dropdown-menu' >".$this->createResponsiveNavigation($cat_row['subcategories'],$cat_url)."</ul>";

				} else {
					$out.=$cat_group_li."<li class='$classes dropdown'><a class='content_leftside_parent' href='$link_url' class='$premium_cat_lnk_cls'>".$new_icn_cd.stripslashes($cat_row['details']['post_category_name'])."</a></li>";
				}
			}
		} 
		return $out;
	}	
	
	
}
?>