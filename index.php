<?php
session_start();

ini_set("display_errors", 0);
error_reporting(0);
/**
 * Define document paths
 */
define('SERVER_ROOT' , dirname(__FILE__));
define('SITE_URL' , $_SERVER['HTTP_HOST'].'/');
require SERVER_ROOT.'/alanee_lib/config/conf.php';
require SERVER_ROOT.'/alanee_lib/config/router.php';
?>
