<?php include('header.php');?>

<script language="javascript">
    $(document).ready(function() {
        $('#add_post').submit(function() {
            var ret = true;
            $('#writer_error').html('');  $('#desc_error').html('');$('#mainCat_error').html(''); $('#desc_error').html('');$('#title_error').html('');$('#post_error').html('');
           // $('#post_head_error').html('');$('#sub_head_error').html('');
            var mainCat  = $('#main_cat').val();
            var writer      =   $('#copy_writer').val();  
            var title      =  $('#post_title').val();  
            var shortdesc  =  $('#short_desc').val();
            var subHead     = $('#sub_head').val();
            var postHead    = $('#post_head').val();
            var post       =  $('#vid_desc').val();  
            
            if(mainCat == ''){
                $('#mainCat_error').html('Please select category.');
                ret =  false;
            }
            
             if(writer == ''){
                $('#writer_error').html('Please select a copy writer.');
                ret =  false;
            }
            
             if(title == ''){
                $('#title_error').html('Please enter post title.');
                ret =  false;
            }
            
             if(shortdesc == ''){
                $('#desc_error').html('Please enter short Description.');
                ret =  false;
            }
            
            /* if(postHead == ''){
                $('#post_head_error').html('Please enter post heading.');
                ret =  false;
            }
            
            if(subHead == ''){
                $('#sub_head_error').html('Please enter sub heading.');
                ret =  false;
            }*/
            
            var editorcontent = CKEDITOR.instances['vid_desc'].getData().replace(/<[^>]*>/gi, '');
             
            if(editorcontent.length){}
            else{
            $('#post_error').html('Please enter post');
            ret =  false;
            }
             if(ret == false){
                $('#writer_error').focus();
                ret =  false;
            }
            
            return ret;
     });
    });
    function Checkfiles()
	{
		var fup = document.getElementById('postImageUpload');
		var fileName = fup.value;
		var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
		var size = fup.files[0].size;
		if(ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG")
		{
			return true;
		} 
		else
		{
			$('#postImage_error').html('Upload Jpg or png images only');
			fup.focus();
			return false;
		}
		if(size > MAX_SIZE){
			$('#postImage_error').html("Maximum file size exceeds");
			fup.focus();
			return false;

		}else{
			return true;
		}	
	}
    
</script>

<?php
$errorMsg = '';
$insertArray = array();

$postTypeArr  = array("N"=>"News","P"=>"Page");
 
$id = $_REQUEST['id'];

if($id == ''){
    header("Location:listpost.php");
}

$myPosts = $postObj->getPostDetails($id);
$insertArray['post_category_id'] = $myPosts[0]['post_category_id'];
$insertArray['copywriter_id'] = stripslashes($myPosts[0]['copywriter_id']);
$insertArray['post_title'] = stripslashes($myPosts[0]['post_title']);
$insertArray['post_cms_small'] = stripslashes($myPosts[0]['post_cms_small']);
$insertArray['post_heading'] = stripslashes($myPosts[0]['post_heading']);
$insertArray['post_subheading'] = stripslashes($myPosts[0]['post_subheading']);
$insertArray['post_released'] = stripslashes($myPosts[0]['post_released']);
$insertArray['post_image'] = stripslashes($myPosts[0]['post_image']);
$insertArray['post_cms'] = stripslashes(cleanMyCkEditor($myPosts[0]['post_cms']));
$insertArray['post_meta_title'] = stripslashes($myPosts[0]['post_meta_title']);
$insertArray['post_share_title'] = stripslashes($myPosts[0]['post_share_title']);
$insertArray['post_share_description'] = stripslashes($myPosts[0]['post_share_description']);
$insertArray['post_meta_keywords'] = stripslashes($myPosts[0]['post_meta_keywords']);
$insertArray['post_meta_description'] = stripslashes($myPosts[0]['post_meta_description']);
$insertArray['post_url'] = $myPosts[0]['post_url'];
$insertArray['post_url_key'] = $myPosts[0]['post_url_key'];

$postType = stripslashes($myPosts[0]['post_type']);

//echo "post is ;".$insertArray['post_cms'];

//echo "insertArry " .$insertArray['post_category_id'];

if(isset($_POST['delImage'])){
	$deleteImageRes = $postObj->deletePostImage($id);
	if($deleteImageRes == 1){
		$file = "../public/uploads/postImages/".$insertArray['post_image'];
		unlink($file);
		header('Location:editpost.php?id='.$id);
		exit();
	}
}

if(isset($_POST['postAdd'])){
      
	if($_POST['postAdd']){
				if(!empty($_FILES['postImageUpload']['name'])){
					$errors= array();
					$file_name = $_FILES['postImageUpload']['name'];
					$file_size = $_FILES['postImageUpload']['size'];
					$file_tmp = $_FILES['postImageUpload']['tmp_name'];
					$file_type = $_FILES['postImageUpload']['type'];
					$extension = explode('.',$_FILES['postImageUpload']['name']);
					$file_ext = $extension[1];

					$extensions = array("jpeg","jpg","png");

					if(in_array($file_ext,$extensions)=== false){
						$errors[]="extension not allowed, please choose a JPEG or PNG file.";
					}

					if($file_size > 2097152) {
						$errors[]='File size must not exceed 2 MB';
					}

										if(empty($errors)==true) {
					if($file_ext=="jpg" || $file_ext=="jpeg" )
					{
						$uploadedfile = $_FILES['postImageUpload']['tmp_name'];
						$src = imagecreatefromjpeg($uploadedfile);
					}
					else if($file_ext=="png")
					{
						$uploadedfile = $_FILES['postImageUpload']['tmp_name'];
						$src = imagecreatefrompng($uploadedfile);
					}
					else 
					{
						$src = imagecreatefromgif($uploadedfile);
					}
					list($width,$height)=getimagesize($uploadedfile);

					$newwidth=640;
					$newheight=360;
					$tmp=imagecreatetruecolor($newwidth,$newheight);

					//$newwidth1=180;
					//$newheight1=110;
					//$tmp1=imagecreatetruecolor($newwidth1,$newheight1);

					imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
					//imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

					$filename = "../public/uploads/postImages/". $_FILES['postImageUpload']['name'];
					//$filename1 = "../public/uploads/postImages/small". $_FILES['postImageUpload']['name'];

					imagejpeg($tmp,$filename,100);
					//imagejpeg($tmp1,$filename1,100);

					imagedestroy($src);
					imagedestroy($tmp);
					//imagedestroy($tmp1);

					//if(empty($errors)==true) {
						//move_uploaded_file($file_tmp,"../public/uploads/postImages/".$file_name);
					}else{
						$errors[] = "File upload failed.";
						print_r($errors);
						exit();
					}
				}
				else{
					$file_name = $insertArray['post_image'];
				}
				
				$updateId = $id;
                $category   = $_POST['main_cat'];
                $writer     = $_POST['copy_writer'];
                $postTitle  = trim($_POST['post_title']);
                $shortDesc  = $_POST['short_desc'];
                $postHead   = addslashes($_POST['post_head']);
                $subHead    = $_POST['sub_head'];
                $postReleased = $_POST['post_released'];
				$postImage = $file_name;
                $post       = $_POST['vid_desc'];
                $postType   = $_POST['post_type'];

				$meta_title = $_POST['meta_title'];	
				$share_title = $_POST['share_title'];
				$meta_keywords = $_POST['meta_keywords'];
				$meta_description = $_POST['meta_description'];
				$share_description = $_POST['share_description'];
                
                $postTitle  = addslashes(cleanInputField($postTitle));
                $post       = addslashes(cleanMyCkEditor($post));
                $shortDesc  = addslashes(clearTextArea($shortDesc));
                $postHead   = trim(cleanInputField($postHead));
                $subHead    = trim(cleanInputField($subHead));
                $postReleased = cleanInputField($postReleased);
				$postImage = cleanInputField($file_name);
		   /*    
            $get_title = str_replace(array(' ',	"'",':','/','\\'), '-', $postTitle);
			$get_title = str_replace(array(',','?','(',')',), '', $get_title);
			$get_title = str_replace(array('%',), 'per', $get_title);
			$get_title = trim(strtolower($get_title),'-');
			*/
            $get_title = $_POST['post_url'];
			$key = md5($get_title);
			
                
                $category  = cleanInputField($category);
                $writer    = cleanInputField($writer);
            
                $post      = cleanInputField($post);          
                $insertArray['post_category_id'] = $category;
                $insertArray['copywriter_id'] = $writer;
                $insertArray['post_title'] = $postTitle;
                $insertArray['post_cms'] = $post;
                $insertArray['post_url'] = $get_title;
				$insertArray['post_url_key'] = $key;
                $insertArray = cleanInputArray($insertArray);
                
                if($category == '' || $writer == '' || $postTitle == '' || $shortDesc == ''  || $post == ''){
			$errorMsg ='Please enter all mandatory fields<br/>';
                       
		} else{
			   //$postObj->updatePost($updateId,$category,$writer,$postTitle,$shortDesc,$postHead,$subHead,$postReleased, $post,$postType);
		        try {
		        	$postObj->updatePost2($updateId,$category,$writer,$postTitle,$shortDesc,$postHead,$subHead,$postReleased,$postImage,$post,$postType, $meta_title, $meta_keywords, $meta_description,$share_description, $get_title, $key,$share_title);
					$successMsg = "Post added successfully.";
                    $insertArray = array(); 
                    echo $successMsg;
                    header('Location:listPost.php');
		        }catch (Exception $ex) {
		        	echo $ex->getMessage();
		        }
			
		}	
		
		
	}
}
?>
<script type="text/javascript" src="../public/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../public/plugins/ckeditor/ckfinder.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">


<div id="page-heading"><h1>Edit Post</h1></div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><img src="<?php echo $adminThemeLink?>themes/theme1/images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><img src="<?php echo $adminThemeLink?>themes/theme1/images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
	
		<!-- start id-form -->
                 <form action="" name="add_post" id="add_post" method="post" enctype="multipart/form-data">
                     <?php if($errorMsg !='') { ?>
				<div class="error_sent_notification"><?php echo $errorMsg;?></div>
	             <?php } ?>
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">

                     <tr>
                            <th valign="top"> Main Categories:</th>
                            <td>
                                <?php  $getCats = $catObj->getAllCategories();?>
                                    
                                <select name="main_cat" id="main_cat"  class="styledselect_form_1">
                                    <option value="">Select Category</option>
                                  <?php  
                                    if(count($getCats)>0){
                                         for($i=0;$i<count($getCats);$i++){
                                             $mainCatSelect   = '';
                                             
                                             $mainCatId   = $getCats[$i]['mainCatId'];
                                             $mainCatName =  $getCats[$i]['mainCatName'];
                                             
                                             if($mainCatId == $insertArray['post_category_id'])
                                             {
                                                 $mainCatSelect = 'selected="selected"';
                                             }
                                             
                                         ?>
                                            <option <?php echo $mainCatSelect?> value="<?php echo $getCats[$i]['mainCatId'] ?>"><?php echo $getCats[$i]['mainCatName'] ?></option>
                                            <?php
                                            $getSubCats = $catObj->getSubChildCategory($mainCatId);
                                            if(count($getSubCats)>0){
                                                for($j=0;$j<count($getSubCats);$j++){
                                                     $subCatSelect    = '';
                                                    $subCatId = $getSubCats[$j]['post_category_id'];
                                                    $subCatName = $mainCatName."-".$getSubCats[$j]['post_category_name'];
                                                    
                                                       if($subCatId == $insertArray['post_category_id'])
                                                        {
                                                            $subCatSelect = 'selected="selected"';
                                                        }
                                                    
                                             ?>
                                             <option <?php echo $subCatSelect ?> value="<?php echo $subCatId ?>"><?php echo $subCatName;?></option>
                                            <?php
                                                $getSubSubCats = $catObj->getSubChildCategory($subCatId);
                                                      for($k=0;$k<count($getSubSubCats);$k++){
                                                          $subSubCatSelect = '';
                                                       $subSubCatId   = $getSubSubCats[$k]['post_category_id']; 
                                                       $subSubCatName =  $subCatName."--".$getSubSubCats[$k]['post_category_name']; 
                                                       
                                                        if($subSubCatId == $insertArray['post_category_id'])
                                                        {
                                                            $subSubCatSelect = 'selected="selected"';
                                                        }
                                                      ?>
                                                       <option <?php echo $subSubCatSelect?>  value="<?php echo $subSubCatId ?>"><?php echo $subSubCatName;?></option>
                                                 <?php
                                                      }
                                                }
                                              }
                                            
                                            }
                                    }
                                    ?>
                                    
		           </select>
                                 <label for="mainCat" class="error" id="mainCat_error"></label>
                            </td>
                            <td></td>
                    </tr>
                    
                     <tr>
                            <th valign="top">Copy writers</th>
                            <td>
                                <?php  $getWriters = $copywriterObj->getActiveCopyWriters();?>
                                    
                                <select name="copy_writer" id="copy_writer"  class="styledselect_form_1">
                                    <option value="">Select Copy writer</option>
                                  <?php  
                                    if(count($getWriters)>0){
                                         for($i=0;$i<count($getWriters);$i++){
                                             $copySelected = '';
                                             if($insertArray['copywriter_id'] == $getWriters[$i]['copywriter_id']){
                                                 $copySelected = 'selected = "selected"';
                                             }
                                         ?>
                                            <option <?php echo $copySelected ?> value="<?php echo $getWriters[$i]['copywriter_id'] ?>"><?php echo $getWriters[$i]['copywriter_user'] ?></option>
                                       <?php                                                
                                            }
                                    }
                                    ?>
                                    
		           </select>
                                 <label for="copy" class="error" id="writer_error"></label>
                            </td>
                            <td></td>
                    </tr>

                    <tr>
                            <th valign="top"> Post Title:</th>
                            <td>
                                <input type="text" name="post_title" value="<?php echo $insertArray['post_title']?>"  id="post_title" class="inp-form2" />
                                <label for="post_title" class="error" id="title_error"></label>
                            </td>
                            <td></td>
                    </tr>
                    <tr>
                            <th valign="top"> Post URL:</th>
                            <td>
                                <input type="text" name="post_url" id="post_url" class="inp-form2" value="<?php echo $insertArray['post_url'];?>" />
                                <br><font color="#ff0000">Note: Existing link won't work if changed.</font>
                                <label for="post_url" class="error" id="post_url_error"></label>
                            </td>
                            <td></td>
                    </tr>                     
                     <tr>
                            <th valign="top"> Post Heading:</th>
                            <td>
                                 <input type="text" name="post_head" id="post_head" value="<?php echo $insertArray['post_heading'] ?>" class="inp-form" />
                                <label for="post_head" class="error" id="post_head_error"></label>
                            </td>
                            <td></td>
                    </tr>
                    
                      
                    
                    
                    <tr>
                            <th valign="top"> Post Sub heading:</th>
                            <td>
                                 <input type="text" name="sub_head" id="sub_head" value="<?php echo $insertArray['post_subheading'] ?>" class="inp-form" />
                                <label for="sub_head" class="error" id="sub_head_error"></label>
                            </td>
                            <td></td>
                    </tr>
                    
                     <tr>
                            <th valign="top"> Post Released:</th>
                            <td>
                                 <input type="text" name="post_released" value="<?php echo $insertArray['post_released'] ?>" id="post_released" class="inp-form" />
                                <label for="post_released" class="error" id="post_released"></label>
                            </td>
                            <td></td>
                    </tr>
                    
                     <tr>
                            <th valign="top"> Short Description:</th>
                            <td>
                                 <textarea name="short_desc" id="short_desc" rows="" cols="" class="form-textarea"><?php echo $insertArray['post_cms_small']?></textarea>
                                <label for="shortDesc" class="error" id="desc_error"></label>
                            </td>
                            <td></td>
                    </tr>
                    
                     <tr>
                            <th valign="top"> Post Image:</th>
                            <td>
							<?php if(!empty($insertArray['post_image'])){ ?>
								<a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/public/uploads/postImages/<?php echo $insertArray['post_image']; ?>" target="_blank">Image Preview</a>
								<input type="submit" name="delImage" onchange="Checkfiles()" value="Delete" class="inp-submit" />
							<?php }else { ?>	
                               <input type="file" name="postImageUpload" id="postImageUpload" class="inp-form" />   
							<?php } ?>	
                            </td>
                            <td></td>
                    </tr>
					
                     <tr>
                            <th valign="top"> Post</th>
                            <td>
                                <textarea  name="vid_desc" id="vid_desc" cols="5" rows="8"  ><?php echo $insertArray['post_cms']?></textarea>
						<script type="text/javascript">
						   if ( typeof CKEDITOR == 'undefined' ){}
						   else{
							var editor = CKEDITOR.replace( 'vid_desc',{
										height:"<?php echo CKH;?>", width:"<?php echo CKW;?>"
										} );
							CKFinder.SetupCKEditor( editor, '../public/plugins/ckeditor/' ) ;
						   }
						  </script>
                                <label for="post" class="error" id="post_error"></label>
                            </td>
                            <td></td>
                    </tr>

                    <tr>
                            <th valign="top">Meta Title:</th>
                            <td>
                                 <input type="text" name="meta_title" id="meta_title" class="inp-form" maxlength="250" value="<?php echo $insertArray['post_meta_title'];?>" />
                            </td>
                            <td></td>
                    </tr>
					 <tr>
                            <th valign="top">Share Title:</th>
                            <td>
                                 <input type="text" name="share_title" id="share_title" class="inp-form" maxlength="250" value="<?php echo $insertArray['post_share_title'];?>" />
							</td>
                            <td></td>
                    </tr>
					<tr>
                            <th valign="top">Share Description:</th>
                            <td>
                                 <input type="text" name="share_description" id="share_description" class="inp-form2" maxlength="1024" 
								 value="<?php echo $insertArray['post_share_description'];?>" />
                            </td>
                            <td></td>
                    </tr>
                    <tr>
                            <th valign="top">Meta Keywords:</th>
                            <td>
                                 <input type="text" name="meta_keywords" id="meta_keywords" class="inp-form2" maxlength="250" value="<?php echo $insertArray['post_meta_keywords'];?>" />
                            </td>
                            <td></td>
                    </tr>
                    <tr>
                            <th valign="top">Meta Description:</th>
                            <td>
                                 <input type="text" name="meta_description" id="meta_description" class="inp-form2" maxlength="1024" value="<?php echo $insertArray['post_meta_description'];?>" />
                            </td>
                            <td></td>
                    </tr>
                    
                      <tr>
                    
                     <tr>
                            <th valign="top">Post Type</th>
                            <td>
                                <?php $newPstArray = array_keys($postTypeArr); ?>
                                    
                                <select name="post_type" id="post_type"  class="styledselect_form_1">
                                    
                                  <?php  
                                    if(count($newPstArray)>0){
                                         for($i=0;$i<count($newPstArray);$i++){
                                             $selected = '';
                                             if($postType == $newPstArray[$i] ){
                                                 $selected = 'selected="selected"';
                                             }
                                         ?>
                                            <option value="<?php echo $newPstArray[$i]?>" <?php echo $selected?>><?php echo $postTypeArr[$newPstArray[$i]]; ?></option>
                                       <?php                                                
                                            }
                                    }
                                    ?>
                                    
		           </select>
                                 <label for="copy" class="error" id="writer_error"></label>
                            </td>
                            <td></td>
                    </tr>

                   

                <tr>
                    <th>&nbsp;</th>
                    <td valign="top">
                            <input type="submit" value="Submit" name="postAdd" class="form-submit" />

                    </td>
                    <td></td>
                </tr>

                </table>
          </form>
	<!-- end id-form  -->

	</td>
	<td>


</td>
</tr>

</table>
 
<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>



<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<?php include('footer.php');?>
