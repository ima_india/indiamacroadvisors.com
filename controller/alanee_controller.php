<?php
if ( ! defined('SERVER_ROOT')) exit('No direct script access allowed');
class AlaneeController extends AlaneeCore {
	public $httpsRootPath;
	
	public function AlaneeController($controllername='',$action='',$params=''){
		parent::AlaneeCore($controllername,$action,$params);
		$this->httpsRootPath = 'https:'.$this->rootPath;
		if(isset($_COOKIE['jmacrm']))
		{
			$rm = base64_decode($_COOKIE['jmacrm']);
			$rmb = base64_decode($rm);
			$remember = explode("_",$rmb);
			$name = $remember[1];
			$pass = $remember[2];
			$user = new User();
			$userDetails = $user->getUserDetailsByUserNameAndPassword($name,$pass);
			$userDetails['password'] = '********';
			if(count($userDetails)>0 && $userDetails['id'] >0) {
				$_SESSION['user'] = $userDetails;
			}	
		}
	}
	
	protected function populateLeftMenuLinks() {
		$postCategory = new postCategory();
		$folder = new Userfolders();
		$navigation = new Navigation();
		$categories = $postCategory->getAllCategory();
		if( $this->isUserLoggedIn() ) {
			$folderList = $folder->getFolder($_SESSION['user']['id']);
		////	echo '<pre>';
		//	print_r($folderList);
		//	exit;
			$folders =  $navigation->createFolderNav($folderList,$this->controllername,$this->action);
		}else{
			$folderList = array();
			$folders = '';
		}
		$left_menu = $navigation->createLeftNavigation($categories);
		  $Responsive_left_menu = $navigation->createResponsiveNavigation($categories);
		$this->renderResultSet['result']['category']['Responsive_left_menu'] = $Responsive_left_menu;
		$this->renderResultSet['result']['category']['menu'] = $left_menu;
		$this->renderResultSet['result']['category']['folders'] = $folders;
		$this->renderResultSet['result']['category']['folderList'] = $folderList;
	}
	
	protected function isUserLoggedIn() {
		if(isset($_SESSION['user']) && $_SESSION['user']['id'] > 0) {
			return true;
		}else{
			return false;
		}
	}

	protected function getAllMyChartFolders() {
		$navigation = new Navigation();
		$folder = new Userfolders();
		if( $this->isUserLoggedIn() ) {
			return $navigation->createFolderNav($folder->getFolder($_SESSION['user']['id']));

		}
		
	}
	
	protected function handleUnpaidUser(){
		if(isset($_SESSION['user']) && $_SESSION['user']['id'] > 0) {
			if($_SESSION['user']['user_status'] == 'unpaid' && $_SESSION['user']['user_type'] == 'individual'){
				$this->redirect('user/user_pay_downgrade');
			}
		}
	}


	
	
}


?>