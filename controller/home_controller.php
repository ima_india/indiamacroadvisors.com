<?php
if ( ! defined('SERVER_ROOT')) exit('No direct script access allowed');
class HomeController extends AlaneeController {
	public $classes = array('alanee_classes/access_management/acl_class.php','alanee_classes/common/alaneecommon_class.php','alanee_classes/common/navigation_class.php');
	public function index() {
		$this->handleUnpaidUser();
		$this->pageTitle = "Unbiased Opinion on India's Economy | IMA offers independent analysis and easy access to economic data";
		$this->renderResultSet['meta']['shareTitle']="Unbiased Opinion on India's Economy | IMA offers independent analysis and easy access to economic data";
		$this->renderResultSet['meta']['description']="India Macro Advisors offers an independent and unbiased economic analysis on the Japanese economy. Through our forecast, regularly updated economic commentaries, over 2000 downloadable economic data and free use of interactive charts, we aim to contribute to an open and evidence-based debate on India's economic future.";
		$this->renderResultSet['meta']['keywords']='Abenomics, India economy, India economic analysis, India economic indicator, India economic policy, Bank of India Monetary policy, India GDP';
		// get all category items
		$postCategory = new postCategory();
		$media = new Media();
		$this->renderResultSet['result']['rightside']['notice'] = $media->getLatestMediaAsNotice(5);
		$this->renderResultSet['result']['rightside']['media'] = $media->getLatestMedia(5);
		$AlaneeCommon = new Alaneecommon();
	//	if(count($this->renderResultSet['result']['rightside']['notice'])>0) {
			/*foreach ($this->renderResultSet['result']['rightside']['notice'] as &$rwn) {
				$rwn['media_value_text'] = $rwn['media_value_text']; //$AlaneeCommon->editorfix($rwn['media_value_text']);
			}*/
	//	}
	//	if(count($this->renderResultSet['result']['rightside']['media'])>0) {
			/*
			foreach ($this->renderResultSet['result']['rightside']['media'] as &$rwm) {
				$rwm['media_value_text'] = $rwm['media_value_text']; // $AlaneeCommon->editorfix($rwm['media_value_text']);
			}*/
	//	}
		$this->populateLeftMenuLinks();
		$post = new Post();
		$this->renderResultSet['result']['news'] = $post->getLatestNewsItems(5);
	//	echo '<pre>';
	//	print_r($this->renderResultSet['result']['news']);
	//	exit;
		$homepagegraph = new Homepagegraph();
		$homepage_graph_details = $homepagegraph->getHomepageGraph();
		$homepageGraph_title = $homepage_graph_details['title'];
		$homepageGraph_description = $homepage_graph_details['description'];
		$homepageGraph_publish_date = $homepage_graph_details['published_date'];
		$homepageGraph_report_url = $homepage_graph_details['report_link'];
		$homepageGraph_code = $homepage_graph_details['graph_code'];
		$homePageGraphContent = "<div class='dv_home_mn_graph_new_ico_txt'><div class='main-title'><h4><span class='dhmg_whamew'>What's New</span>$homepageGraph_title</h4> <div class='mttl-line'></div> </div> <p>$homepageGraph_description <i>($homepageGraph_publish_date)</i></p> </div>
		<div style='padding-bottom:15px;font-size:12px;'><a href='".$this->url($homepageGraph_report_url)."'>Click here for the full version of comments and chart functions.</a></div>";
		$homePageGraphContent .= $AlaneeCommon->makeChart($homepageGraph_code,false);
		$this->renderResultSet['result']['homepagegraph'] = $homePageGraphContent;
		$this->renderView();
	}
	
	public function admin_index() {
		$this->renderView();
	}
	
}

?>