<?php
if ( ! defined('SERVER_ROOT')) exit('No direct script access allowed');
class Graphdetails extends AlaneeModal {
	
	public function getGraphDetailsForTheseComaseperatedGids($gids) {
		$response = array();
		$sql = "select gid, title from graph_details where gid in ($gids)";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			//$response = $rs->fetch_all(true);
			while ($rw = $rs->fetch_assoc()) {
				$response[] = $rw;
			}	
		} 
		return $response;
	}
	
	public function getMapDetailsForTheseComaseperatedGids($gids) {
		$response = array();
		$sql = "SELECT DISTINCT(x_value) FROM map_values WHERE gid IN ($gids)";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			//$response = $rs->fetch_all(true);
			while ($rw = $rs->fetch_assoc()) {
				$response[] = $rw;
			}	
		} 
		return $response;
	}
	
	
	public function getMapDataDetailsForGids($gids) {
		$response = array();
		$sql = "SELECT DISTINCT(y_sub_value) FROM map_values WHERE gid IN  ($gids)";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			//$response = $rs->fetch_all(true);
			while ($rw = $rs->fetch_assoc()) {
				$response[] = $rw;
			}	
		}
		return $response;
	}
	
	public function getMapDetailsForJsonDataAll($gids) {
		
		$responseArr = "";
		$sql = "SELECT DISTINCT(y_sub_value) FROM map_values WHERE gid IN  ($gids)";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			while ($rw = $rs->fetch_assoc()) {
				$responseArr.= $rw['y_sub_value'].",";
			}	
		}
		$statesReg =  str_replace(",","','",$responseArr);
		$stateVal = trim($statesReg,",'"); //"'".$statesReg; 
		$newStateRange = "'".$stateVal."'"; 
		
		$response = array();
		$sql = "SELECT mv.x_value,mv.value FROM map_values AS mv WHERE gid = '$gids' AND  y_sub_value IN ($newStateRange) GROUP BY y_sub_value,x_value ORDER BY FIELD(y_sub_value,$newStateRange),x_value";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			//$response = $rs->fetch_all(true);
			while ($rw = $rs->fetch_assoc()) {
				$response[] = $rw;
			}	
		}
		return $response;
	}
	
	public function checkIsPremiumForTheseComaseperatedGids($gids) {
		$result = '';
		$sql = "select gid, title, isPremium from graph_details where gid in ($gids) AND isPremium = 'y'";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			$result = true;
		} 
		else {
			$result = false;
		}
		return $result;
	}
	
	public function getGraphSourceForTheseComaseperatedGids($gids){
		$response = array();
		$sql = "select source FROM graph_details where gid in ($gids)";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			//$response = $rs->fetch_all(true);
			while ($rw = $rs->fetch_assoc()) {
				$sources = explode(',',$rw['source']);
				foreach($sources as $source_ky) {
					$source = trim($source_ky);
					if(!in_array($source,$response)){
						$response[] = $source;
					}
				}
			}
		}
		return implode(', ',$response);
	}
	public function getFilepathForTheseComaseperatedGids($gids) {
		$response = array();
		$sql = "select gid, filepath from graph_details where gid in ($gids)";
		$rs = $this->executeQuery($sql);
		if($rs->num_rows>0) {
			//$response = $rs->fetch_all(true);
			while ($rw = $rs->fetch_assoc()) {
				$response[] = $rw;
			}
		} 
		return $response;
	}
	
}
?>